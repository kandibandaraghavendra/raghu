samvit.controller("adminHomeController",[
				'$scope',
				'$route',
				'$http',
				'$rootScope',
				'OAuth',
				'$location',
				function($scope, $route, $http, $rootScope, OAuth,$location){
					$scope.headerTemplate = "views/admin/adminHeader.html";
					
					$scope.startUpdateRecords = function(){
						$http.get('bank/getUpdateRecords').success(
								function(response, status, headers) {
									if (response.status == 'success') {
										alert("Records updating started successfully. please wait for 20 min");
									} else {
									}
								});
					};
					
					//for page init 
					$scope.pageInit = function(){
						$scope.showErrorMessage = false;
						$http.get('request/getAllPendingRequests').success(
								function(response, status, headers) {
									if (response.status == 'success') {
										$scope.allPendingRequests = response.result;
									} else {
										$scope.allPendingRequests = null;
										$scope.showErrorMessage = true;
										$scope.errorMessage = "No Requests are available for approvel";
									}
								});
					};
					
					//for reject call
					$scope.rejectCall = function(requestComments, req){
						alert(requestComments);
						if(requestComments == null || requestComments == "" || requestComments == undefined){
							alert("Please enter rejection message");
						}else{
							var ar = {
									id : req.id,
									requestComments : requestComments
							}
							
							$http({	url : "request/rejectRequest",
								headers : {
											'Content-Type' : 'application/json',
											'Accept' : 'application/json'
										},
								method : "POST",
								data : ar
							}).success(function(returnData) {
								if (returnData.status == "success") {
									$scope.pageInit();
								}else{
									alert("service failed");
								}
							});
							
						}
					};
					
				}]);