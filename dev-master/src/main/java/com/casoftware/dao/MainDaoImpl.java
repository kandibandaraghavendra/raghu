package com.casoftware.dao;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.casoftware.modal.User;

@Repository("mainDao")
public class MainDaoImpl extends AbstractDao implements MainDao {

	@Override
	public List<User> getAllUsers() {
		Session session = getSession();
		return (List<User>) session.createCriteria(User.class).list();
	}

}
